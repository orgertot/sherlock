var clusterdictionary = [],
	selectedIndex,
	ipcRenderer = window.ipcRenderer

function loadClusterDictionary(callback) {
	return fetch("http://10.10.40.5:9200/clusterdictionary/_search?size=10000",
	{
		method: "GET",
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json'
		}
	})
	.then(function(response) {
		return response.json();
	})
	.then(function(data){
		data.hits.hits.forEach(function(hit) {
			hit._source._id = hit._id
			clusterdictionary.push(hit._source)
		})
		callback()
	})
}
function loadIndices(callback) {
	return fetch("http://10.10.40.5:9200/_cat/indices",
	{
		method: "GET",
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json'
		}
	})
	.then(function(response) {
		return response.json();
	})
	.then(function(data){ 
		var indexChoiceDropdown = document.getElementById("index_choice_dropdown");
		data.sort((a,b) => (a.index > b.index) ? 1 : ((b.index > a.index) ? -1 : 0));
		selectedIndex = data[0].index
		data.forEach(function(index) {
			var foundDictionaryEntry = clusterdictionary.find(function(ele) { return ele.indexName == index.index && ele.showInQueryTool })
				indexChoiceDropdown.insertAdjacentHTML('beforeend',`
					<option ` +` value='`+ index.index +`'>`+ (foundDictionaryEntry ? foundDictionaryEntry.displayName : index.index) +`</option>
				`);
		})
		callback()
	})
	.catch(function(error) {
		console.log(error);
	})
}
function changeSelectedIndex(value) {
	selectedIndex = value
	loadFieldsOfIndex()
}
function autoFillFields() {
	var fieldsDiv = document.getElementById("fields");
	Array.prototype.forEach.call(fieldsDiv.getElementsByClassName('fieldsEdit'), function (element) {
		element.getElementsByClassName('displayName')[0].value = element.dataset.field;
		element.getElementsByClassName('showInQueryTool')[0].checked = true;
	});
}
function loadFieldsOfIndex() {
	return fetch("http://10.10.40.5:9200/" + selectedIndex + "/_mapping",
	{
		method: "GET",
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json'
		}
	})
	.then(function(response) {
		return response.json();
	})
	.then(function(data){
		var indexDiv = document.getElementById("index"),
			fieldsDiv = document.getElementById("fields"),
			clusterDictionaryOfIndex = clusterdictionary.find((ele) => ele.indexName == selectedIndex)
		indexDiv.innerHTML = ""
		fieldsDiv.innerHTML = ""
		indexDiv.insertAdjacentHTML('beforeend', `
			<div id="selection" class="indexEdit">
				<div class="pflege_row"><label>Anzeige Name für den Index: </label><input class="displayName" value='`+ (clusterDictionaryOfIndex ? clusterDictionaryOfIndex.displayName : '') +`'></div>
				<div class="pflege_row"><label>In Sherlock anzeigen: </label><input class="showInQueryTool" type="checkbox" `+ (clusterDictionaryOfIndex && clusterDictionaryOfIndex.showInQueryTool ? 'checked' : '') +`></div>
				<div class="pflege_row"><label>Beschreibung: </label><input class="discription" value='`+ (clusterDictionaryOfIndex ? clusterDictionaryOfIndex.discription : '') +`'></div>
			</div>
		`)
		for (var docType in data[selectedIndex].mappings) {
			for (var field in data[selectedIndex].mappings[docType].properties) {
				var clusterDictionaryEntryForField = clusterDictionaryOfIndex ? clusterDictionaryOfIndex.fields.find((ele) => ele.fieldName == field) : undefined;
				fieldsDiv.insertAdjacentHTML('beforeend',`
					<div id="selection" class="fieldsEdit `+(clusterDictionaryEntryForField && /^--/.test(clusterDictionaryEntryForField.displayName) ? 'deleted' : '' )+`" data-field="`+field+`">
						<div class="pflege_row"><label>Feld Name: `+ field +`</label></div>
						<div class="pflege_row"><label>Anzeige Name: </label><input class="displayName" value='`+ (clusterDictionaryEntryForField ? clusterDictionaryEntryForField.displayName : '') +`'></div>
						<div class="pflege_row"><label>In Sherlock anzeigen:</label><input class="showInQueryTool" type="checkbox" `+ (clusterDictionaryEntryForField && clusterDictionaryEntryForField.showInQueryTool ? 'checked' : '') +`></div>
						<div class="pflege_row"><label>Beschreibung: </label><input class="discription" value='`+ (clusterDictionaryEntryForField ? clusterDictionaryEntryForField.discription : '') +`'></div>
						<div class="pflege_row" style='display: none;'><label>Oracle Dev Info:</label><input class="dev_oracleInfo" value='`+ (clusterDictionaryEntryForField ? clusterDictionaryEntryForField.dev_oracleInfo : '') +`'></div>
					</div>
				`);
			}
		}
	})
}
function save() {
	var indexDiv = document.getElementById("index"),
		fieldsDiv = document.getElementById("fields"),
		saveObj = {}

	saveObj.indexName = selectedIndex
	let indexParent = indexDiv.getElementsByClassName('indexEdit')[0]
	saveObj.displayName = indexParent.getElementsByClassName('displayName')[0].value
	saveObj.showInQueryTool = indexParent.getElementsByClassName('showInQueryTool')[0].checked
	saveObj.discription = indexParent.getElementsByClassName('discription')[0].value
	saveObj.fields = []

	Array.prototype.forEach.call(fieldsDiv.getElementsByClassName('fieldsEdit'), function (element) {
		var saveElement = {}
		saveElement.fieldName = element.dataset.field
		saveElement.displayName = element.getElementsByClassName('displayName')[0].value
		saveElement.showInQueryTool = element.getElementsByClassName('showInQueryTool')[0].checked
		saveElement.discription = element.getElementsByClassName('discription')[0].value
		saveElement.dev_oracleInfo = element.getElementsByClassName('dev_oracleInfo')[0].value
		saveObj.fields.push(saveElement)
	});

	let clusterDictionaryOfIndex = clusterdictionary.find((ele) => ele.indexName == selectedIndex)
	
	return fetch("http://10.10.40.5:9200/clusterdictionary/clusterdictionary_doc" + (clusterDictionaryOfIndex ? ("/"+clusterDictionaryOfIndex._id) : ''),
	{
		method: "POST",
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json'
		},
		body: JSON.stringify(saveObj)
	})
	.then(function(response) {
		return response.json()
			.then((json) => {
				if (!response.ok) {
					const error = Object.assign({}, json, {
						status: response.status,
						statusText: response.statusText,
					});

					return Promise.reject(error);
				}
				return json;
			});
	})
	.then(function(data){
		alert("Änderungen gespeichert.")
		ipcRenderer.send('reloadMain', null);
	})
	.catch(function(error) {
		alert("Fehler beim Speichern")
		alert(error)
	})
}

loadClusterDictionary(()=> {
	loadIndices(()=> {
		loadFieldsOfIndex()
	})
})