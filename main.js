// Modules to control application life and create native browser window
const {app, BrowserWindow, ipcMain, dialog, Menu} = require('electron');
const updater = require("electron-updater");
const autoUpdater = updater.autoUpdater;
autoUpdater._logger.debug = true;

const request = require('request'),
      fs = require('fs'),
      Json2csvTransform = require('json2csv').Transform,
      Json2csvParser = require('json2csv').Parser,
      process = require('process'),
      path = require('path'),
      iconv = require('iconv-lite'),
      Store = require('electron-store'),
      store = new Store(),
      prompt = require('electron-prompt'),
      saveLocationTemp = 'temp',
      JSONStream = require('JSONStream'),
      es = require('event-stream');

autoUpdater.requestHeaders = { "PRIVATE-TOKEN": "sR-vxJbDKKGYCqKRs3JU" };
autoUpdater.autoDownload = true;

autoUpdater.setFeedURL({
  provider: "generic",
  url: "https://gitlab.com/orgertot/sherlock/-/jobs/artifacts/master/raw/dist?job=build"
})

var mainWindow,
  clusterdictionary = []
setAppMenu();
loadClusterDictionary();
function createWindow () {
  // Create the browser window.
  mainWindow = new BrowserWindow({
    width: 1000, 
    height: 800,
    show: true,
    webPreferences: {
      nodeIntegration: false,
      preload: path.join(__dirname, 'preload.js')
    }
  });
  mainWindow.maximize();
  mainWindow.focus();
  mainWindow.loadFile('index.html')
  mainWindow.once('ready-to-show', () => {
    //win.show()
  });
  mainWindow.on('closed', function () {
    mainWindow = null;
  });
  autoUpdater.checkForUpdatesAndNotify().then((value) => {
    console.dir(value)
    sendStatusToWindow('checkForUpdatesAndNotify new2');
    sendStatusToWindow(JSON.stringify(value.updateInfo));
    sendStatusToWindow(JSON.stringify(value.versionInfo));
  }).catch(function(error) {
    sendStatusToWindow(JSON.stringify(error));
  })
}
function readMeineAbfragen() {
  var abfragen = store.get('abfragen', []),
      returnValue = [{
        label: 'Abfrage speichern',
        click() { saveAbfrage() }
      }, { label: 'Abfragen löschen', submenu: readMeineAbfragenDelete()},{type: 'separator'}]
  abfragen.forEach((abfrage) => {
    returnValue.push({
      label: abfrage.name,
      click() { openAbfrage(abfrage.query) }
    })
  })
  return returnValue
}
function readMeineAbfragenDelete() {
  var abfragen = store.get('abfragen', []),
      returnValue = []
  abfragen.forEach((abfrage) => {
    returnValue.push({
      label: abfrage.name,
      click() { deleteAbfrage(abfrage.name, abfrage.query) }
    })
  })
  return returnValue
}
function saveAbfrage() {
  mainWindow.webContents.send('getQuery')
}
function deleteAbfrage(name, query) {
  var abfragen = store.get('abfragen', [])
  for (let index = 0; index < abfragen.length; index++) {
    const element = abfragen[index];
    if(element.name == name && element.query == query.toString())
      abfragen.splice(index, 1)
  }
  console.log(abfragen)
  store.set('abfragen', abfragen)
  setAppMenu()
}
function loadClusterDictionary() {
  request.post({
    url: 'http://10.10.40.5:9200/clusterdictionary/_search?size=10000',
    method: 'GET',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }
  }, function(error, response, body) {
    JSON.parse(body).hits.hits.forEach(function(hit) {
			hit._source._id = hit._id
			clusterdictionary.push(hit._source)
    })
  })
}
function setAppMenu() {
  var template = [
    {
      label: 'Start',
      submenu: [
        { label: 'Beenden', role: 'quit' }
      ]
    },
    {
      label: 'Meine Abfragen',
      submenu: readMeineAbfragen()
    },
    {
      label: 'Bearbeite Dictionary',
      click() { openPflege() }
    },
    {
      label: 'Dashboards',
      submenu: [
        { label: 'Abfrage67', click() { openKibanaDashboard(`http://10.10.40.5:5601/app/kibana#/dashboard/785a8040-e1b4-11e8-bb86-1daa0f20d641?_g=(refreshInterval%3A('$$hashKey':'object:231',display:'10%20seconds',pause:!f,section:1,value:10000)%2Ctime%3A(from:now-5y,mode:relative,to:now))&_a=(description:'',filters:!(),fullScreenMode:!t)`) }}
      ]
    },
    {
      label: 'Entwickler-Modus',
      role: 'toggledevtools'
    }
  ],
  menu = Menu.buildFromTemplate(template);
  Menu.setApplicationMenu(menu)
}
function openPflege() {
  var url = require('url')
  let newWindow = new BrowserWindow({
    width: 800, 
    height: 600,
    webPreferences: {
      nodeIntegration: false,
      preload: path.join(__dirname, 'preload.js')
    }
  })
  newWindow.loadURL(url.format({
    slashes: true,
    protocol: 'file:',
    pathname: path.resolve(app.getAppPath(), 'pflege.html'),
  }));
  newWindow.maximize()
  newWindow.focus()
}
function openAbfrage(query) {
  var url = require('url')
  mainWindow.loadURL(url.format({
    slashes: true,
    protocol: 'file:',
    pathname: path.resolve(app.getAppPath(), 'index.html'),
    query: {
      query: encodeURIComponent(JSON.stringify(query).replace(/,"bcChannel":\d*/g, ""))
    }
  }));
}
function openKibanaDashboard(url) {
  let newWindow = new BrowserWindow({width: 800, height: 600})
  newWindow.loadURL(url)
  newWindow.maximize()
  newWindow.focus()
  newWindow.webContents.on('did-finish-load', function() {
    newWindow.webContents.insertCSS('.exitFullScreenButton { display: none; }')
  });
}
app.on('ready', createWindow)
app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})
app.on('activate', function () {
  if (mainWindow === null) {
    createWindow()
  }
})
ipcMain.on('exportExcel', (event, arg) => {
  console.log(new Date().getTime())
  
  var index = arg.index,
      query = arg.query,
      fields = arg.headings,
      exportFileName = path.join(app.getPath(saveLocationTemp),'export'+new Date().getTime()+'.csv'),
      token

  console.log(fields)
  console.log(query)
  
  query = JSON.parse(query)
  var aggregation = (query["aggs"] && query["aggs"] != {})
  console.log("aggs", aggregation)
  if(!aggregation)
    query.size = 10000
  if(aggregation) {
    query.size = 0
    query = addSizesToTermAggs(query)
  }
  query = JSON.stringify(query)

  var clusterDictionaryOfIndex = clusterdictionary.find((ele) => ele.indexName == index)
  fields.forEach(function(field, index) {
    var clusterDictionaryEntryForField = clusterDictionaryOfIndex ? clusterDictionaryOfIndex.fields.find((ele) => ele.fieldName == field) : undefined;
    if(clusterDictionaryEntryForField !== undefined) {
      fields[index] = {
        label: clusterDictionaryEntryForField.displayName,
        value: field
      }
    }
  })

  console.dir(fields)

  var mainRequest = request.post({
    url: 'http://10.10.40.5:9200/'+ index +'/_search?scroll=5m',
    method: 'POST',
    body: query,
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    gzip: true
  });
  mainRequest.pipe(JSONStream.parse('_scroll_id'))
  .pipe(es.mapSync(function (data) {
    token = data
  })).on('finish', function() {}).on('error', err => console.log(err));

  var fileName = 'temp'+new Date().getTime()+'.json';
  mainRequest.pipe(fs.createWriteStream(path.join(app.getPath(saveLocationTemp),fileName), { encoding: 'utf-8' }))
  .on('finish', function() {
    if(aggregation) {
      return convertAggregationFileToCSV(fileName)
    } else {
      return convertFileToCSV(fileName)
    }
  })
  .on('error', err => console.log(err));

  function readNextScroll(scrollID) {
    var fileName = 'temp'+new Date().getTime()+'.json';
    request.post({
      url: 'http://10.10.40.5:9200/_search/scroll',
      method: 'POST',
      body: `{"scroll": "5m", "scroll_id": "`+scrollID+`"}`,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      gzip: true
    })
    .on('error', function(err) {
      alert(err)
    })
    .pipe(fs.createWriteStream(path.join(app.getPath(saveLocationTemp),fileName)))
    .on('finish', function() {
      convertFileToCSV(fileName, false)
    })
    .on('error', err => console.log(err));
  }
  function convertFileToCSV(fileName, includeheader) {
    console.log("REQUEST DONE")
    if(includeheader == null || includeheader == undefined) includeheader = true
    var foundData = false;
    fs.createReadStream(path.join(app.getPath(saveLocationTemp),fileName), { encoding: 'utf8' })
    .pipe(JSONStream.parse('hits.hits.*'))
    .pipe(es.mapSync(function (data) {
      foundData = true;
      data = JSON.stringify(data)
      data = data.replace(/(?::)(\d+?)\.(\d+?)(?:,)/g, ":\"$1,$2\",")
      data = JSON.parse(data)
      var returnData = JSON.stringify(Object.assign({}, data._source, data.fields));
      return returnData;
    }))
    .pipe(new Json2csvTransform({ fields, delimiter: ";", includeEmptyRows: false, header: includeheader}))
    .pipe(iconv.decodeStream('utf-8'))
    .pipe(iconv.encodeStream('win1252'))
    .pipe(fs.WriteStream(exportFileName, { flags: "a"}))
    .once('finish', function () {
      fs.unlink(path.join(app.getPath(saveLocationTemp),fileName), function(error) {
        if(error)
          console.log(error)
        console.log("DONE")
        if(foundData) {
          readNextScroll(token)
        } else {
          fs.copyFile(exportFileName, path.join(app.getPath('desktop'),'export'+new Date().getTime()+'.csv'), function(error) {
            if(error) console.log(error)
            fs.unlink(exportFileName, function(error) {
              if(error) console.log(error)
              console.log(new Date().getTime())
              dialog.showMessageBox({message: "Export abgeschlossen. Die Excel befindet sich auf dem Desktop."})
            })
          })
        }
      })
    }).on('error', err => console.log(err));
  }
  function convertAggregationFileToCSV(fileName) {
    fs.readFile(path.join(app.getPath(saveLocationTemp),fileName), { encoding: 'utf8' },function(error, data) {
      if(error)
        return console.log(error)

            data = data.replace(/(?::)(\d+?)\.(\d+?)(?:,)/g, ":\"$1,$2\",")
      data = tabify(JSON.parse(data))

      var csv = new Json2csvParser({ fields, delimiter: ";", includeEmptyRows: false }).parse(data)

      csv = iconv.encode(csv, 'win1252')

      fs.writeFile(exportFileName, csv, { flags: "a" }, function(error) {
        if(error)
          return console.log(error)
        fs.unlink(path.join(app.getPath(saveLocationTemp),fileName), function(error) {
          fs.copyFile(exportFileName, path.join(app.getPath('desktop'),'export'+new Date().getTime()+'.csv'), function(error) {
            if(error) console.log(error)
            fs.unlink(exportFileName, function(error) {
              if(error) console.log(error)
              console.log(new Date().getTime())
              dialog.showMessageBox({message: "Export abgeschlossen. Die Excel befindet sich auf dem Desktop."})
            })
          })
        })
      })
    })
  }
  function addSizesToTermAggs(obj) {
    if(obj && obj.aggs) {
      var ele = Object.keys(obj.aggs)[0]
      if(Object.keys(obj.aggs[ele])[0] && Object.keys(obj.aggs[ele])[0] == "terms")
        obj.aggs[ele][Object.keys(obj.aggs[ele])[0]].size = 999999;
      if(Object.keys(obj.aggs[ele])[1] && Object.keys(obj.aggs[ele])[1] == "terms")
        obj.aggs[ele][Object.keys(obj.aggs[ele])[1]].size = 999999;
      obj.aggs[ele] = addSizesToTermAggs(obj.aggs[ele]);
    }
    return obj
  }
});
ipcMain.on('saveQuery', (event, arg) => {
  var query = arg;
  prompt({
    title: 'Abfrage speichern',
    label: 'Name der Abfrage:',
    value: '',
    inputAttrs: {
        type: 'text'
    },
    type: 'input'
  })
  .then((input) => {
      if(input !== null) {
        var abfragen = store.get('abfragen', [])
        abfragen.push({
          'name': input,
          'query': query
        })
        store.set('abfragen', abfragen)
        setAppMenu()
      }
  })
  .catch(console.error);
})
ipcMain.on('reloadMain', () => {
  mainWindow.reload()
  loadClusterDictionary()
})
/*######## Aggregation Export ######*/
function tabify(response, options) {
  let table;
  if (typeof (options) === 'undefined') {
    options = {
      debug: false
    }
  }

  if (response.aggregations) {
    const tree = collectBucket(response.aggregations);
    table = flatten(tree);

  } else if (response.hits) {
    table = response.hits.hits.map((d) => d._source);

  } else if (Array.isArray(response)) {
    table = response;

  } else {
    throw new Error("Tabify() invoked with invalid result set. Result set must have either 'aggregations' or 'hits' defined.");
  }

  if (options.debug) {
    console.log("Results from tabify (first 3 rows only):");

    // This one shows where there are "undefined" values.
    console.log(table)

    // This one shows the full structure pretty-printed.
    console.log(JSON.stringify(table.slice(0, 3), null, 2))
  }

  return table;
}
function collectBucket(node, stack=[]) {
  if (!node)
    return;
  
  const keys = Object.keys(node);
  
  // Use old school `for` so we can break control flow by returning.
  for(let i = 0; i < keys.length; i++) {
    const key = keys[i];
    const value = node[key];
    if (typeof value === 'object' && value !== null) {
      if ("hits" in value && Array.isArray(value.hits) && value.hits.length === 1) {
        if ("sort" in value.hits[0]) {
          value.hits[0]._source['sort'] = value.hits[0].sort[0];
        }
        return value.hits[0]._source;
      }

      if (Array.isArray(value)) {
        return extractTree(value, [...stack, key]);
      }

      // Here we are sure to have an object
      if (key === "buckets" && Object.keys(value).length > 1)
      {
        return extractBuckets(value, [...stack, key]);
      }

      return collectBucket(value, [...stack, key]);
    }

    if (key === "value" && typeof value !== "object" && stack.length === 1) {
      let collectedObject = collectBucket({[stack[0]]: value});
      node = collectedObject;
    }
  }

  return node;
}
function extractBuckets(buckets, stack) {
  const keys = Object.keys(buckets);
  let results = [];

  for(let i = 0; i < keys.length; i++) {
    const key = keys[i];
    const value = buckets[key];

    let currentObject = collectBucket({[key]: value});

    if (!currentObject)
      continue;

    currentObject[stack[stack.length - 2]] = key;
    results.push(currentObject)
  }

  return results;
}
function extractTree(buckets, stack) {
  return buckets.map((bucket) => {
    return Object.keys(bucket).reduce(function (tree, key) {
      let value = bucket[key];

      if (typeof value === "object") {
        if("value" in value){
          value = value.value;
        } else {
          value = collectBucket(value, [...stack, key]);
        }
      }

      if(key === "key"){
        key = stack[stack.length - 2]
      }
      
      tree[key] = value;
    
      return tree;
    }, {});
  });
}
function flatten(tree, parentNode={}){

  if (!tree)
    return [];

  if (!Array.isArray(tree))
    tree = [tree];

  return tree

    // Have the child node inherit values from the parent.
    .map((childNode) => Object.assign({}, parentNode, childNode))

    // Each node object here has values inherited from its parent.
    .map((node) => {

      // Detect properties whose values are arrays.
      const childTrees = Object.keys(node)
        .map((key) => {
          const value = node[key];
          if (Array.isArray(value)) {
            return value;
          }
          return false;
        })
        .filter((d) => d);

      switch (childTrees.length) {

        // Leaf node case, return the node.
        case 0:
          return node;

        // Non-leaf node case, recurse on the child nodes.
        case 1:
          const childTree = childTrees[0];
          if(childTree.length === 0){
            return node;
          }
          return flatten(childTree, node);
        default:
          throw new Error("This case should never happen");
      }
    })

    // Flatten the nested arrays.
    .reduce((a, b) => a.concat(b), []);
}
/*#########################*/
autoUpdater.on('checking-for-update', function () {
  sendStatusToWindow('Checking for update...');
});
autoUpdater.on('update-available', function (info) {
  sendStatusToWindow('Update available.');
});
autoUpdater.on('update-not-available', function (info) {
  sendStatusToWindow('Update not available.');
});
autoUpdater.on('error', function (err) {
  sendStatusToWindow('Error in auto-updater.');
});
autoUpdater.on('download-progress', function (progressObj) {
  let log_message = "Download speed: " + progressObj.bytesPerSecond;
  log_message = log_message + ' - Downloaded ' + parseInt(progressObj.percent) + '%';
  log_message = log_message + ' (' + progressObj.transferred + "/" + progressObj.total + ')';
  sendStatusToWindow(log_message);
});
autoUpdater.on('update-downloaded', function (info) {
  sendStatusToWindow('Update downloaded; will install in 1 seconds');
});
autoUpdater.on('update-downloaded', function (info) {
  setTimeout(function () {
      autoUpdater.quitAndInstall();
  }, 1000);
});
function sendStatusToWindow(message) {
  dialog.showMessageBox(mainWindow, { message: message })
}