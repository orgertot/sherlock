
var query = [[/* where */],[/* aggs */], [/* show fields */], /* index*/ "abfrage67v3"],
	printedQuery = "", //Actual Query to send
	references = [],
	clusterdictionary = [],
	fieldsOfIndex = [],
	showFieldsOfIndex = [],
	aggableFieldsOfIndex = [],
	nestedFields = {},
	fieldDescription = {},
	isASubQuery = false,
	outgoingBC,
	allResultsMaximum = 320000,
	ipcRenderer = window.ipcRenderer,
	id = 0,
	preID = Math.floor(Math.random()*(999-100+1)+100);

	if(getQueryVariable("query")) {
		query = JSON.parse(decodeURIComponent((new URLSearchParams(window.location.search)).get('query')));
	}
	if(getQueryVariable("subQuery")) {
		isASubQuery = true;
		outgoingBC = new BroadcastChannel("querytool"+getQueryVariable("subQuery"));
		outgoingBC.postMessage({
			subQuery: query,
			subQueryResult: ""
		});
		transformToSubQueryUI();
	}
	function drawQueryForm() {
		references = [];
		drawWhereForm();
		drawAggsForm();
	}
	function transformToSubQueryUI() {
		document.getElementsByTagName("body")[0].style.width = "50%";
		document.getElementsByTagName("body")[0].style.margin = "auto";
		document.getElementById("page_content").style.padding = "10px 0px 0px 0px";
	}
	function drawWhereForm() {
		var whereDiv = document.getElementById("where");
		whereDiv.innerHTML = "";
		whereDiv.insertAdjacentHTML('beforeend',render(query[0]));
		console.log("-----")
		console.log(query[0])
		console.log("-----")
		function render(array, nestedParent) {
			var endHtmlCode = "";
			references.push(array);
			var arrayreference = references[references.length - 1],
				arrayreferenceKey = (references.length - 1);
			for (let index = 0; index < array.length; index++) {
				var element = array[index];
				references.push(element);
				var myreference = references[references.length - 1],
					myreferenceKey = (references.length - 1);
				if(myreference.type === "term") {
					endHtmlCode += `
						<div class="row"">
							`+ (nestedFields[element.field] ? '<div class="group"><div class="group_term">' : '') +`
							<select onchange="references[` + myreferenceKey + `].field = this.value; printQuery(); returnHtmlDataListForField(this.value, 'field` + myreferenceKey +`'); drawQueryForm();">
							` 
								+ returnHtmlFieldOptions(references[myreferenceKey].field, nestedParent) + 
							`
							</select>` + (nestedFields[element.field] ? '</div>' : '');
					if(!nestedFields[element.field]) {
						endHtmlCode += `	
								<select onchange="references[` + myreferenceKey + `].key = this.value; drawQueryForm(); printQuery()">
									<option `+ (myreference.key == "term" ? "selected" : "") +` value="term">=</option>
									<option `+ (myreference.key == ">" ? "selected" : "") +` value=">">></option>
									<option `+ (myreference.key == "<" ? "selected" : "") +` value="<"><</option>
									<option `+ (myreference.key == "match" ? "selected" : "") +` value="match">Like</option>
									<option `+ (myreference.key == "exists" ? "selected" : "") +` value="exists">ist nicht leer</option>
									<option `+ (myreference.key == "wildcard" ? "selected" : "") +` value="wildcard">Wildcard</option>
									<option `+ (myreference.key == "script" ? "selected" : "") +` value="script">Script</option>
									<option `+ (myreference.key == "equalfield" ? "selected" : "") +` value="equalfield">= anderes Feld</option>
									<option `+ (myreference.key == "ifnullthen" ? "selected" : "") +` value="ifnullthen">wenn leer benutze</option>
								</select>`;
						if(myreference.key != "exists" && myreference.key != "equalfield" && myreference.key != "ifnullthen") {
							endHtmlCode += `
									<input placeholder="Wert" value="` + myreference.value + `" onkeyup="references[` + myreferenceKey + `].value = this.value; printQuery()" id="afield`+ myreferenceKey +`" list="field`+ myreferenceKey + `" ` + (myreference.options.subQuery ? 'disabled' : '') +`>
									` + (myreference.key == "term" ? `<small style="cursor:pointer;margin-left: 5px;background-color: `+(myreference.options.subQuery ? '#31b677' : '#999')+`;height: 14px;font-size: 10px;box-sizing: content-box;padding: 4px;margin-top: 6px;color: #fff;" onclick="createSubQuery('`+ myreferenceKey +`')">` + (myreference.options.subQuery ? '<b>Sub-Query</b>' : 'Sub-Query') +`</small>` : '') + `
									` + returnHtmlDataListForField(myreference.field, "field"+myreferenceKey);
						} else if(myreference.key == "equalfield" || myreference.key == "ifnullthen") {
							endHtmlCode += `
							<select onchange="references[` + myreferenceKey + `].value = this.value; printQuery(); returnHtmlDataListForField(this.value, 'field` + myreferenceKey +`'); drawQueryForm();">
							` 
								+ returnHtmlFieldOptions(references[myreferenceKey].value, nestedParent) + 
							`
							</select>`;

							if(myreference.key == "ifnullthen") {
								if(myreference.options.name == undefined) myreference.options.name = "";
								endHtmlCode += `<label style="position: relative;margin: 0px 4px;line-height: 31px;">als</label><input placeholder="Bezeichnung für die Ausgabe" value="` + myreference.options.name + `" onkeyup="references[` + myreferenceKey + `].options.name = this.value; printQuery()">`;
							}
						}
					} else {
						if(!myreference.options.nestedFieldsArray)
							myreference.options.nestedFieldsArray = [];
						endHtmlCode += render(myreference.options.nestedFieldsArray, myreference.field)
					}
					endHtmlCode += `
						`+ (nestedFields[element.field] ? '</div>' : '') +`
						<button class="remove" onclick='removeColoumn(`+element.id+`)'>x</button>
						</div>
					`;
				}
				if(myreference.type === "group") {
					var htmlString = 
					`
						<div class="row">
							<div class="group">
								<div class="group_term">
									<select onchange="references[` + myreferenceKey + `].key = this.value; drawQueryForm(); printQuery()">
										<option value="must" `+ (myreference.key == "must" ? "selected" : "") +`>And</option>
										<option value="should" `+ (myreference.key == "should" ? "selected" : "") +`>Or</option>
										<option value="must_not" `+ (myreference.key == "must_not" ? "selected" : "") +`>And Not</option>
									</select>
									<button class="remove" onclick='removeColoumn(`+element.id+`)'>x</button>
								</div>
					`;
					htmlString += render(myreference.value, nestedParent);
					htmlString += `
							</div>
						</div>
					`;
					endHtmlCode += htmlString;
				}			
			}
			endHtmlCode += '<div class="row"><div class="addKey" onclick="addTerm(references[' + arrayreferenceKey + '], \''+(nestedParent ? nestedParent : '')+'\')">+ Term</div><div class="addKey" onclick="addGroup(references[' + arrayreferenceKey + '])">+ Group</div></div>';
			return endHtmlCode;
		}
	}
	function addTerm(parent, nestedtParentFieldName) {
		id++;
		console.log(nestedtParentFieldName != '' ? nestedFields[nestedtParentFieldName][Object.keys(nestedFields[nestedtParentFieldName])[0]]: '')
		parent.push({
			id: preID.toString() + id,
			type: "term",
			key: "term",
			field: (nestedtParentFieldName != '' ? Object.keys(nestedFields[nestedtParentFieldName])[0] : fieldsOfIndex[0]),
			value: "",
			options: {}
		})
		drawQueryForm();
		printQuery();
	}
	function addGroup(parent) {
		id++;
		parent.push({
			id: preID.toString() + id,
			type: "group",
			key: "must",
			value: []
		});
		drawQueryForm();
		printQuery();
	}
	function escapeValueCharacters(value) {
		return value.replace(new RegExp("\"", "g"), "\\\"");
	}
	function printQuery (allResults, callback) {	
		var queryDiv = document.getElementById("query"),
			whereQuery = "",
			scriptFields = {};
		queryDiv.innerHTML = "";
		function recursionWhere(row, allResults, nestedParent) {
			var endQuery = "";
			for (let index = 0; index < row.length; index++) {
				const element = row[index];

				if(element.type == "term") {
					if(index != 0 && row[index-1] && row[index-1].key != "ifnullthen") endQuery += ",";

					//Nested Felder - Öffende Klammern
					if(nestedFields[element.field]) {
						endQuery += '{"nested" : {"path" : "'+ element.field +'","query" : {"bool" : {"must" : [';
						if(element.options.nestedFieldsArray)
							endQuery += recursionWhere(element.options.nestedFieldsArray, null, element.field)
						endQuery += ']}}}}';
					//Script
					} else if(element.key == "script") {
						endQuery += '{ "script": {"script": "'+ escapeValueCharacters(element.value) +'"}}';
					//Else If
					} else if(element.key == "ifnullthen") {
						var feldOderKeywordFeld = (fieldsOfIndex.indexOf(element.field + ".keyword") > -1 ? element.field + ".keyword" : element.field)

						var feldOderKeywordFeld2 = (fieldsOfIndex.indexOf(element.value + ".keyword") > -1 ? element.value + ".keyword" : element.value)

						scriptFields[element.options.name] = {};
						scriptFields[element.options.name].script = {};
						scriptFields[element.options.name].script.source = "doc['"+feldOderKeywordFeld+"'].values.length > 0 ? doc['"+feldOderKeywordFeld+"'].value : doc['"+feldOderKeywordFeld2+"'].values.length > 0 ? doc['"+feldOderKeywordFeld2+"'].value : null";
					//= anderes Feld
					} else if(element.key == "equalfield") {
						endQuery += `{ "script": {"script": "doc['`+ element.value +`'].value == doc['`+ element.field +`'].value"}}`;
					//Kleiner
					} else if(element.key == "<") {
						endQuery += '{ "range": {"' + (nestedParent ? nestedParent + "." + element.field : element.field) +'": {"lt":"'+ element.value +'"}}}';
					//Größer
					} else if(element.key == ">") {
						endQuery += '{ "range": {"' + (nestedParent ? nestedParent + "." + element.field : element.field) +'": {"gt":"'+ element.value +'"}}}';
					//Exists / Not NUll
					} else if(element.key == "exists") {
						endQuery += '{"exists": { "field": "'+ (nestedParent ? nestedParent + "." + element.field : element.field) +'"}}';
					//Der Rest
					} else if(element.options.subQuery) {
						endQuery += '{"terms": {"'+ (nestedParent ? nestedParent + "." + element.field : element.field) +'":'+ element.value.replace(/&quot;/g, '"') +'}}';
					} else {
						//Wir verstecken alle .keyword felder
						//Aber wenn man ein text feld = x sucht, kommt da nur mist raus
						//Daher verwenden wir in dem Fall das Keyword-Feld, wenn es das gibt
						var feldOderKeywordFeld
						if(element.key == "term"  || element.key == "wildcard")
							feldOderKeywordFeld = (fieldsOfIndex.indexOf(element.field + ".keyword") > -1 ? element.field + ".keyword" : element.field)
						else feldOderKeywordFeld = element.field;

						endQuery += '{"' + element.key +'": {"'+ (nestedParent ? nestedParent + "." + feldOderKeywordFeld : feldOderKeywordFeld) +'":"'+ escapeValueCharacters(element.value) +'"}}';
					}
				}
				if(element.type == "group") {
					if(index != 0) endQuery += ",";
					endQuery += `{
						"bool": {
							"`+ element.key +`": [
								` + recursionWhere(element.value, null, nestedParent) + `
							]
						}
					}`
				}
			}
			return endQuery;
		}
		function recursionAggs(row, allResults) {
			var endQuery = "";
			for (let index = 0; index < row.length; index++) {
				const element = row[index];
				if(element.type == "metric") {
					//if(index != 0 || (query[1].length == 1 && query[1][0].type == "metric")) endQuery += ",";
					endQuery +=',"aggs":{';
					if(element.key == "sum")
						endQuery += `"sum_`+ element.field +`" : { "sum" : { "field" : "`+ element.field +`" } }`
					endQuery += "}";
				}
				if(element.type == "bucket") {
					endQuery += `
						,"aggs": {
							"`+ element.key + "_" + element.field  + index + `": {
								"`+ element.key +`": {
									`+ (allResults && element.key != "range" ? '"size": 2147483647,' : '') +`
									"field": "`+ element.field +`"`;
					if(element.key == "histogram") {
						endQuery += ',"interval": ' + element.options.interval
						if(element.options.hideNulls)
							endQuery += ',"min_doc_count": 1';
					}
					if(element.key == "range") {
						endQuery += `,"ranges" : [`; 
						element.options.range.sort(function(a, b) {return a - b;});
						for (let index2 = 0; index2 < element.options.range.length; index2++) {
							const ele = element.options.range[index2];
							if(index2 == element.options.range.length - 1) {
								//letztes element
								endQuery += `{ "to" : `+ ele +` }`
							} else {
								endQuery += `{ "from" : `+ele+`, "to" : `+ element.options.range[index2+1]+` },`
							}
						}
						endQuery += `]`; 
					} 
					endQuery += `}` + (recursionAggs(element.value).length != 0 ? '' + recursionAggs(element.value) : '') + `
							}
						}
					`;
				}
			}
			return endQuery;
		}
		function checkForSubQueries(array, allResults, callback) {
			function aSyncWaitForCallLoop(array, index, func) {
				if(index < array.length) {
					func(array[index++], function() {
						aSyncWaitForCallLoop(array, index++, func);
					});
				} else {
					callback();
				}
			}
			//console.log(array)
			if(array.length > 0) {
				aSyncWaitForCallLoop(array, 0, function(element, callback) {
					if(element.type == "group") {
						//console.log("element is group")
						if(element.value.length > 0) {
							//Has Childs
							checkForSubQueries(element.value, allResults,function() {
								//Okay Done. Sub Queries for Child executed
								//Now do Mine
								callback();
							})
						} else {
							//Has No Child. So no Subqueries. Continue
							//console.log("No Childs")
							callback();
						}
					}
					if(element.type == "term") {
						if(element.options.subQuery) {
							generatePrintedQuery(element.options.subQuery, allResults,function(result) {
								fetch("http://10.10.40.5:9200/"+element.options.subQuery[3]+"/_search" + (element.options.subQuery[1].length > 0 ? "?size=0" : "?size=100"),
								{
									method: "POST",
									headers: {
										'Accept': 'application/json',
										'Content-Type': 'application/json'
									},
									body: result
								})
								.then(function(response) {
									return response.json();
								})
								.then(function(data){ 
									//console.log("FINISHED SUBQUERY");
									var tabifyData = tabify(data);
									console.log("HI")
									console.log(flattenArray(tabifyData))
									console.log("HUI")
									element.value = JSON.stringify(uniqueArray(flattenArray(tabifyData.map(function(element) { return element[Object.keys(element)[0]]; })))).replace(/"/g, '&quot;').replace(/(,\s*)null(?=,)/g, "$1&quot;&quot;");
									//console.log("RESULT: ", element.value);
									callback();
								})
								.catch(function(error) {
									console.log(error)
								})
							})
						} else {
							//console.log("I DONT HAVE A SUBQUERY");
							callback();
						}
					}
				});
			} else {
				//console.log("EXIT")
				callback();
			}
		}
		function generatePrintedQuery(query, allResults, callback) {
			var endPrinted = "";
			checkForSubQueries(query[0], allResults, function() {
				//console.log("DONE NOW Final Query")
				whereQuery = recursionWhere(query[0], allResults);
				aggsQuery = recursionAggs(query[1], allResults);
				endPrinted = 
				`{
					"query": {
						"bool":  {
							"filter": {
								"bool": {
									"must": [
										` + whereQuery + `
									]
								}
							}
						}
					},
					`+(scriptFields != {} ? `"script_fields": ` + JSON.stringify(scriptFields)+ `,` : ``)+`
					"sort": [
						"_doc"
					]
					`+ aggsQuery +`
					,"_source": ` + JSON.stringify(query[2]) +`
				}`;
				return callback(endPrinted);
			});
		}

		generatePrintedQuery(query, allResults, function(result) {
			printedQuery = result;
			if(!allResults)
				sendRequest();
			if(callback)
				callback();
		});
	}
	function sendRequest() {
		var resultTable = document.getElementById("resultTable");
		resultTable.innerHTML = '<center><div class="single4"></div></center>';
		document.getElementById("count").innerText = ""
		
		fetch("http://10.10.40.5:9200/"+query[3]+"/_search" + (query[1].length > 1 ? "?size=0" : "?size=20"),
		{
			method: "POST",
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(JSON.parse(printedQuery), null, 4)
		})
		.then(function(response) {
			return response.json()
			.then((json) => {
				if (!response.ok) {
					const error = Object.assign({}, json, {
						status: response.status,
						statusText: response.statusText,
					});

					return Promise.reject(error);
				}
				return json;
			});
		})
		.then(function(data){ 
			var tabifyData = tabify(data);
			document.getElementById("count").innerText = tabifyData.length + " von " + data.hits.total + " docs";
			if(tabifyData.length == 0) {
				resultTable.innerHTML = '<center>Kein Datensatz gefunden</center>';
			} else {
				drawResultTable(tabifyData);
			}

			if(isASubQuery) {
				console.log("Sende Broad")
				console.log(JSON.stringify(query));
				outgoingBC.postMessage({
					subQuery: JSON.parse(JSON.stringify(query)),
					subQueryResult: tabifyData.map(function(element) { return element[Object.keys(element)[0]]; }),
				});
			}
			tabifyData = "";
		})
		.catch(function(error) {
			document.getElementById("count").innerText = ""
			resultTable.innerHTML = "<b>Fehler in der Abfrage.</b><br><br>" + JSON.stringify(error.error);
		})
	}
	function sendAllRequest(onlyReturn, callback) {
		var resultDiv = document.getElementById("result");
		resultDiv.innerHTML = '<center><div class="single4"></div></center>';
		var resultTable = document.getElementById("resultTable");
		resultTable.innerHTML = '<center><div class="single4"></div></center>';
		document.getElementById("count").innerText = ""
		printQuery(true, function() {
			if(!onlyReturn) {
				fetch("http://10.10.40.5:9200/"+query[3]+"/_search" + (query[1].length > 0 ? "?size=0" : "?size="+allResultsMaximum),
				{
					method: "POST",
					headers: {
						'Accept': 'application/json',
						'Content-Type': 'application/json'
					},
					mode: 'no-cors',
					body: JSON.stringify(JSON.parse(printedQuery), null, 4)
				})
				.then(function(response) {
					return response.json()
					.then((json) => {
						if (!response.ok) {
							const error = Object.assign({}, json, {
								status: response.status,
								statusText: response.statusText,
							});

							return Promise.reject(error);
						}
						return json;
					});
				})
				.then(function(data){ 
					console.log(data.error);
					if(data.error) {
						document.getElementById("resultTable").innerHTML= JSON.stringify(data.error.reason)
					} else {
						var tabifyData = tabify(data);
						document.getElementById("count").innerText = tabifyData.length + " docs";
						resultDiv.innerHTML = JSON.stringify(tabifyData, null, 4);
					}
					if(!onlyReturn && !data.error)
						drawResultTable(tabifyData);
					drawQueryForm();

					if(isASubQuery) {
						console.log("Sende Broad")
						console.log(JSON.stringify(query));
						outgoingBC.postMessage({
							subQuery: JSON.parse(JSON.stringify(query)),
							subQueryResult: tabifyData.map(function(element) { return element[Object.keys(element)[0]]; }),
						});
					}
					if(onlyReturn)
						return callback(tabifyData);
					tabifyData = "";
				})
				.catch(function(error) {
					console.log(error);
				})
			} else {
				fetch("http://10.10.40.5:9200/"+query[3]+"/_search?size=100",
				{
					method: "POST",
					headers: {
						'Accept': 'application/json',
						'Content-Type': 'application/json'
					},
					body: JSON.stringify(JSON.parse(printedQuery), null, 4)
				})
				.then(function(response) {
					return response.json()
					.then((json) => {
						if (!response.ok) {
							const error = Object.assign({}, json, {
								status: response.status,
								statusText: response.statusText,
							});

							return Promise.reject(error);
						}
						return json;
					});
				})
				.then(function(data){
					var tabifyData = tabify(data);
					var headings = []
					for (var i = 0; i < tabifyData.length; i++) {
						for(var index in tabifyData[i]) {
							if(headings.indexOf(index) == -1) {
								headings.push(index)
							}
						}
					}
					console.log(query)
					var eventData = {
						index: query[3],
						query: JSON.stringify(JSON.parse(printedQuery), null, 4),
						headings: headings
					}
					ipcRenderer.send('exportExcel', eventData);
					resultTable.innerHTML = "<b>Excel Export läuft im Hintergrund..</b>";
				}).catch(function(error) {
					resultDiv.innerHTML = error.error;
					document.getElementById("count").innerText = ""
					if(false) {

					} else {
						resultTable.innerHTML = "<b>Fehler in der Abfrage.</b><br><br>" + JSON.stringify(error.error);
					}
				})
			}
		});
	}
	function addBucketAggregation(parent) {
		id++;
		parent.push({
			id: preID.toString() + id,
			type: "bucket",
			key: "terms",
			field: fieldsOfIndex[0],
			options: {},
			value: []
		})
		drawQueryForm();
		printQuery();
	}
	function addMetricAggregation(parent) {
		id++;
		parent.push({
			id: preID.toString() + id,
			type: "metric",
			key: "sum",
			field: "",
			options: {}
		})
		drawQueryForm();
		printQuery();
	}
	function drawAggsForm() {
		var aggsDiv = document.getElementById("aggs");
		aggsDiv.innerHTML = "";
		console.log(JSON.stringify(query[1]))
		aggsDiv.insertAdjacentHTML('beforeend',render(query[1]));

		function render(array) {
			var endHtmlCode = "";
			references.push(array);
			var arrayreference = references[references.length - 1],
				arrayreferenceKey = (references.length - 1);
			for (let index = 0; index < array.length; index++) {
				var element = array[index];
				references.push(element);
				var myreference = references[references.length - 1],
					myreferenceKey = (references.length - 1)
				
				if(myreference.type === "bucket") {
					endHtmlCode += `
						<div class="row"">
							<div class="group">
									<div class="group_term">
										<select onchange="references[` + myreferenceKey + `].key = this.value; drawQueryForm(); printQuery()">
											<option `+ (myreference.key == "terms" ? "selected" : "") +` value="terms">Gruppiere nach Wert</option>
											<option `+ (myreference.key == "range" ? "selected" : "") +` value="range">Gruppiere nach Zahlenbereichen</option>
											<option `+ (myreference.key == "histogram" ? "selected" : "") +` value="histogram">Gruppiere in Zahleninterval</option>
										</select>
										<label>für Werte im Feld</label>
										<select onchange="references[` + myreferenceKey + `].field = this.value; printQuery();">
										` + returnHtmlAggableFieldOptions(references[myreferenceKey].field) + ` 
										</select>
										`;
					if(myreference.key === "terms") {
						//Nothing todo here
					}
					if(myreference.key === "histogram") {
						if(!myreference.options.interval || myreference.options.interval == "")
							myreference.options.interval = 1;
						endHtmlCode += `<label>im Interval</label><input type="number" onchange="references[` + myreferenceKey + `].options.interval = this.value; printQuery();" value="` + references[myreferenceKey].options.interval + `" style="width: 75px">`;
						endHtmlCode += `<input type="checkbox" onchange="references[` + myreferenceKey + `].options.hideNulls = !references[` + myreferenceKey + `].options.hideNulls; printQuery();" `+ (references[myreferenceKey].options.hideNulls ? 'checked': '') +`><label>Leere Intervalle ausblenden</label>`;
					}
					if(myreference.key === "range") {
						if(!myreference.options.range || myreference.options.range == [])
							myreference.options.range = [0, 100, 200]
						endHtmlCode += `
							<label>in den Zahlenbereichen von/bis<label>
							<input onchange="references[` + myreferenceKey + `].options.range = this.value.split(','); printQuery();" value="` + 
							(references[myreferenceKey].options.range ? references[myreferenceKey].options.range.join() : '') + `">
						`;
					}
					endHtmlCode += `
								<button class="remove" onclick='removeColoumn(`+element.id+`)'>x</button>
								</div>
								`+ render(myreference.value) +`
							</div>
						</div>
					`;
				}
				if(myreference.type === "metric") {
					endHtmlCode += `
						<div class="row"">
							<select onchange="references[` + myreferenceKey + `].key = this.value; drawQueryForm(); printQuery()">
								<option `+ (myreference.key == "sum" ? "selected" : "") +` value="sum">Summe</option>
							</select>`;
					if(myreference.key === "sum") {
						endHtmlCode += `
							<select onchange="references[` + myreferenceKey + `].field = this.value; printQuery()">
								` + returnHtmlAggableFieldOptions(references[myreferenceKey].field) + ` 
							</select>
						`;
					}
					endHtmlCode += `
							<button class="remove" onclick='removeColoumn(`+element.id+`)'>x</button>
						</div>
					`;
				}

			}
			if(query[1].length == 0 || myreference == undefined || myreference && myreference.type == "metric") {
				endHtmlCode += '<div class="row">'
				endHtmlCode += '<div class="addKey" onclick="addMetricAggregation(references[' + arrayreferenceKey + '])">+ Metrische Operation</div>'
				if(query[1].length == 0 || myreference == undefined)
					endHtmlCode += '<div class="addKey" onclick="addBucketAggregation(references[' + arrayreferenceKey + '])">+ Gruppiert</div>'
				endHtmlCode += '</div>';

			}
			return endHtmlCode;
		}
	}
	function getClusterdictionary(callback) {
		return fetch("http://10.10.40.5:9200/clusterdictionary/_search?size=10000",
		{
			method: "GET",
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			}
		})
		.then(function(response) {
			return response.json();
		})
		.then(function(data){
			data.hits.hits.forEach(function(hit) {
				clusterdictionary.push(hit._source)
			})
			console.log(clusterdictionary)
			callback()
		})
	}
	function fillIndicesDropdown() {
		return fetch("http://10.10.40.5:9200/_cat/indices",
		{
			method: "GET",
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			}
		})
		.then(function(response) {
			return response.json();
		})
		.then(function(data){ 
			var indexChoiceDropdown = document.getElementById("index_choice_dropdown");
			data.sort((a,b) => (a.index > b.index) ? 1 : ((b.index > a.index) ? -1 : 0)); 
			data.forEach(function(index) {
				var foundDictionaryEntry = clusterdictionary.find(function(ele) { return ele.indexName == index.index && ele.showInQueryTool })
				if(foundDictionaryEntry != undefined)
					indexChoiceDropdown.insertAdjacentHTML('beforeend',`
						<option `+ (index.index == query[3] ? 'selected' : '') +` value='`+ index.index +`'>`+ foundDictionaryEntry.displayName +`</option>
					`);
			})
		})
		.catch(function(error) {
			console.log(error);
		})
	} 
	function changeSelectedIndex(indexName) {
		query = [[],[],[], indexName];
		references = [];
		printedQuery = "";
		fillFieldsOfIndex(function() {
			fillAggableFieldsOfIndex(function() {
				drawQueryForm();
				query[2] = fieldsOfIndex;
				drawShowFieldsInResult();
				printQuery();
			});
		});
	}
	function fillFieldsOfIndex(callback) {
		fieldsOfIndex = [];
		showFieldsOfIndex = [];
		return fetch("http://10.10.40.5:9200/" + query[3] + "/_mapping",
		{
			method: "GET",
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			}
		})
		.then(function(response) {
			return response.json();
		})
		.then(function(data){
			var clusterDictionaryOfIndex = clusterdictionary.find((ele) => ele.indexName == query[3])
			for (var docType in data[query[3]].mappings) {
				if(data[query[3]].mappings[docType]._meta && data[query[3]].mappings[docType]._meta.fieldDescription)
					fieldDescription = Object.assign(fieldDescription, data[query[3]].mappings[docType]._meta.fieldDescription);
				for (var field in data[query[3]].mappings[docType].properties) {
					if(data[query[3]].mappings[docType].properties[field].type == "nested") { //Nested?
						nestedFields[field] = data[query[3]].mappings[docType].properties[field].properties;
					}
					if(data[query[3]].mappings[docType].properties[field].fields) { //Fields
						for(var fieldFields in data[query[3]].mappings[docType].properties[field].fields) {
							fieldsOfIndex.push(field + "." + fieldFields);
						}
					}
					var clusterDictionaryEntryForField = clusterDictionaryOfIndex ? clusterDictionaryOfIndex.fields.find((ele) => ele.fieldName == field) : undefined;
					if(clusterDictionaryEntryForField && clusterDictionaryEntryForField.showInQueryTool == true) {
						fieldsOfIndex.push(field);
						showFieldsOfIndex.push(field);
						query[2].push(field)
					}
				}
			}
			fieldsOfIndex.sort(function(aField, bField) {
				aField = clusterDictionaryOfIndex ? (clusterDictionaryOfIndex.fields.find((ele) => ele.fieldName == aField) ? clusterDictionaryOfIndex.fields.find((ele) => ele.fieldName == aField).displayName : aField) : aField;
				bField = clusterDictionaryOfIndex ? (clusterDictionaryOfIndex.fields.find((ele) => ele.fieldName == bField) ? clusterDictionaryOfIndex.fields.find((ele) => ele.fieldName == bField).displayName : bField) : bField;
				if(aField.toUpperCase() < bField.toUpperCase())
					return -1;
				if(aField.toUpperCase() > bField.toUpperCase())
					return 1;
				return 0;
			})
			if(callback != undefined)
				return callback();
		})
		.catch(function(error) {
			console.log(error);
		})
	}
	function fillAggableFieldsOfIndex(callback) {
		aggableFieldsOfIndex = [];
		return fetch("http://10.10.40.5:9200/" + query[3] + "/_mapping",
		{
			method: "GET",
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			}
		})
		.then(function(response) {
			return response.json();
		})
		.then(function(data){
			var clusterDictionaryOfIndex = clusterdictionary.find((ele) => ele.indexName == query[3])
			for (var docType in data[query[3]].mappings) {
				for (var field in data[query[3]].mappings[docType].properties) {
						if(data[query[3]].mappings[docType].properties[field].type == "keyword" || data[query[3]].mappings[docType].properties[field].type == "double")
							aggableFieldsOfIndex.push(field);
						if(data[query[3]].mappings[docType].properties[field].fields) {
							for(var fieldFields in data[query[3]].mappings[docType].properties[field].fields) {
								if(data[query[3]].mappings[docType].properties[field].fields[fieldFields].type == "keyword" || data[query[3]].mappings[docType].properties[field].fields[fieldFields].type == "double")
									aggableFieldsOfIndex.push(field + "." + fieldFields);
							}
						}
				}
			}
			aggableFieldsOfIndex.sort(function(aField, bField) {
				aField = clusterDictionaryOfIndex ? (clusterDictionaryOfIndex.fields.find((ele) => ele.fieldName == aField) ? clusterDictionaryOfIndex.fields.find((ele) => ele.fieldName == aField).displayName : aField) : aField;
				bField = clusterDictionaryOfIndex ? (clusterDictionaryOfIndex.fields.find((ele) => ele.fieldName == bField) ? clusterDictionaryOfIndex.fields.find((ele) => ele.fieldName == bField).displayName : bField) : bField;
				if(aField.toUpperCase() < bField.toUpperCase())
					return -1;
				if(aField.toUpperCase() > bField.toUpperCase())
					return 1;
				return 0;
			})
			if(callback != undefined)
				return callback();
		})
		.catch(function(error) {
			console.log(error);
		})
	}
	function returnHtmlFieldOptions(selectedField, nestedParent) {
		var endHtmlCode = "";
		var clusterDictionaryOfIndex = clusterdictionary.find((ele) => ele.indexName == query[3])
		if(!nestedParent) {
			fieldsOfIndex.forEach(function(field) {
				if(!field.match(new RegExp('.keyword$'))) {
					var clusterDictionaryEntryForField = clusterDictionaryOfIndex ? clusterDictionaryOfIndex.fields.find((ele) => ele.fieldName == field) : undefined;
					if(clusterDictionaryEntryForField && clusterDictionaryEntryForField.showInQueryTool == true)
						endHtmlCode += "<option "+ 
									(selectedField == field ? " selected " : "") + 
									'title="'+ (clusterDictionaryEntryForField && clusterDictionaryEntryForField.discription != "" ? clusterDictionaryEntryForField.discription.replace(/"/g, '&quot;') : 'Keine Beschreibung') +'"'+
									'value="'+ field +'"' +
									">"+ (clusterDictionaryEntryForField && clusterDictionaryEntryForField.displayName != ""  ? clusterDictionaryEntryForField.displayName : field) +"</option>";
				}
			})
		} else {
			for(var field in nestedFields[nestedParent]) {
				endHtmlCode += "<option "+ 
					(selectedField == field ? "selected" : "") + 
					(fieldDescription[nestedParent + "." + field] ? 'title="'+ fieldDescription[nestedParent + "." + field].description +'"' : '') +
					(fieldDescription[field] ? 'value="'+ field +'"' : '') +
					">"+ (fieldDescription[nestedParent + "." + field] ? fieldDescription[nestedParent + "." + field].name : field) +"</option>";
			}
		}
		return endHtmlCode;
	}
	function returnHtmlAggableFieldOptions(selectedField) {
		var endHtmlCode = "";
		var clusterDictionaryOfIndex = clusterdictionary.find((ele) => ele.indexName == query[3])
		aggableFieldsOfIndex.forEach(function(field) {
			var clusterDictionaryEntryForField = clusterDictionaryOfIndex ? clusterDictionaryOfIndex.fields.find((ele) => ele.fieldName == field.replace(/\.keyword$/,"")) : undefined;
			if(clusterDictionaryEntryForField && clusterDictionaryEntryForField.showInQueryTool)
				endHtmlCode += `<option `+ 
								(selectedField == field ? " selected " : "") +
								'title="'+ (clusterDictionaryEntryForField && clusterDictionaryEntryForField.discription != "" ? clusterDictionaryEntryForField.discription : 'Keine Beschreibung') +'"'+
								'value="'+ field +'"'+
							`>`+ 
								(clusterDictionaryEntryForField && clusterDictionaryEntryForField.displayName != "" ? clusterDictionaryEntryForField.displayName : field.replace(/\.keyword$/,"")) +
							`</option>`;
		})
		return endHtmlCode;
	}
	function drawResultTable(data) {
		var resultTable = document.getElementById("resultTable"),
			properties = [];
		
		resultTable.innerText = "";

		data.forEach(function(row) {
			for(var prop in row) { 
				if(properties.indexOf(prop) == -1)
					properties.push(prop);
			}
		})

		var endHtmlCode = `<table id="actualResultTable"><thead><tr>`;
		var clusterDictionaryOfIndex = clusterdictionary.find((ele) => ele.indexName == query[3])
		properties.sort(function(aField, bField) {
			aField = clusterDictionaryOfIndex ? (clusterDictionaryOfIndex.fields.find((ele) => ele.fieldName == aField) ? clusterDictionaryOfIndex.fields.find((ele) => ele.fieldName == aField).displayName : aField) : aField;
			bField = clusterDictionaryOfIndex ? (clusterDictionaryOfIndex.fields.find((ele) => ele.fieldName == bField) ? clusterDictionaryOfIndex.fields.find((ele) => ele.fieldName == bField).displayName : bField) : bField;
			if(aField.toUpperCase() < bField.toUpperCase())
				return -1;
			if(aField.toUpperCase() > bField.toUpperCase())
				return 1;
			return 0;
		})
		properties.forEach(function(prop) {
			var clusterDictionaryEntryForField = clusterDictionaryOfIndex ? clusterDictionaryOfIndex.fields.find((ele) => ele.fieldName == prop) : undefined;
			endHtmlCode += "<th>"+ (clusterDictionaryEntryForField ? clusterDictionaryEntryForField.displayName : prop) +"</th>";
		});
		endHtmlCode += `</tr></thead>`;
		endHtmlCode += `<tbody id="contentArea" class="clusterize-content"></tbody></table>`;
		resultTable.insertAdjacentHTML('beforeend', endHtmlCode);

		var endHtmlCodeArray = []
		data.forEach(function(row) {
			var endHtmlCode = ""
			endHtmlCode += "<tr>";
			properties.forEach(function(prop) {
				if(row.hasOwnProperty(prop)) {
					endHtmlCode += "<td><div>"+ (JSON.stringify(row[prop]) != "null" ? JSON.stringify(row[prop]) : '') +"</div></td>";
				} else {
					endHtmlCode += "<td><div></div></td>";
				}
			})
			endHtmlCode += "</tr>";
			endHtmlCodeArray.push(endHtmlCode);
		})

		clusterize = new Clusterize({
			rows: endHtmlCodeArray,
			scrollId: 'resultTable',
			contentId: 'contentArea'
		});
	}
	function returnHtmlDataListForField(feld, inputId) {
		var endHtmlCode = "";
		//first delete element
		if(document.getElementById(inputId) !== null)
			document.getElementById(inputId).remove();
		if(aggableFieldsOfIndex.indexOf(feld) > -1) {
			fetch("http://10.10.40.5:9200/" + query[3] + "/_search",
			{
				method: "POST",
				headers: {
					'Accept': 'application/json',
					'Content-Type': 'application/json'
				},
				body: `{"size": 0,"aggs": {"term_agg": {"terms": {"field": "`+ feld +`","size": 11}}}}`
			})
			.then(function(response) {
				return response.json();
			})
			.then(function(data){
				if(data.aggregations.term_agg.buckets.length <= 10) {
					endHtmlCode += `<datalist id="`+ inputId +`">`;
					data.aggregations.term_agg.buckets.forEach(function(row) {
						endHtmlCode += `<option value="`+ row.key +`">`+ row.key +` (`+ row.doc_count +`)</option>`;
					});
					endHtmlCode += `</datalist>`;
				}
				document.getElementById("a"+inputId).parentNode.insertAdjacentHTML('beforeend', endHtmlCode);
				return endHtmlCode;
			})
			.catch(function(error) {
				console.log(error);
			})
		}
		return endHtmlCode;
	}
	function drawShowFieldsInResult() {
		var showFieldDiv = document.getElementById("showFields"),
			clusterDictionaryOfIndex = clusterdictionary.find((ele) => ele.indexName == query[3])
			endHtml = "";

		showFieldDiv.innerHTML = "";
		showFieldsOfIndex.sort(function(aField, bField) {
			aField = clusterDictionaryOfIndex ? (clusterDictionaryOfIndex.fields.find((ele) => ele.fieldName == aField) ? clusterDictionaryOfIndex.fields.find((ele) => ele.fieldName == aField).displayName : aField) : aField;
			bField = clusterDictionaryOfIndex ? (clusterDictionaryOfIndex.fields.find((ele) => ele.fieldName == bField) ? clusterDictionaryOfIndex.fields.find((ele) => ele.fieldName == bField).displayName : bField) : bField;
			if(aField.toUpperCase() < bField.toUpperCase())
				return -1;
			if(aField.toUpperCase() > bField.toUpperCase())
				return 1;
			return 0;
		})
		if(!isASubQuery) {
			showFieldsOfIndex.forEach(function(field, index) {
				var clusterDictionaryEntryForField = clusterDictionaryOfIndex ? clusterDictionaryOfIndex.fields.find((ele) => ele.fieldName == field) : undefined;
				if(clusterDictionaryEntryForField && clusterDictionaryEntryForField.showInQueryTool)
					endHtml += `<div><input type="checkbox" id="ci`+index+`" onchange="toogleShowFieldInResult(this.checked, '`+ field +`');  printQuery()" ` +
							(query[2].indexOf(field) > -1 ? 'checked' : '') + `><label for="ci`+ index +`">` + (clusterDictionaryEntryForField && clusterDictionaryEntryForField.displayName != "" ? clusterDictionaryEntryForField.displayName : field) + `</label></div>`;
			})
		} else {
			endHtml += '<select onchange="query[2] = [this.value]; printQuery();">';
			showFieldsOfIndex.forEach(function(field, index) {
				endHtml += `<option `+ (query[2][0] == field ? 'selected' : '') +`>`+ field +`</option>`;
			})
			endHtml += "</select>";
		}

		showFieldDiv.insertAdjacentHTML("beforeend", endHtml);
	}
	function toogleShowFieldInResult(checked, field) {
		if(!checked && query[2].indexOf(field) > -1) {
			query[2].splice(query[2].indexOf(field),1);
		} else if(checked && query[2].indexOf(field) == -1) {
			query[2].push(field);
		}
	}
	function createSubQuery(referenceKey) {
		if(!references[referenceKey].options.bcChannel) {
			console.log("Creating Sub-Query-Channel...");
			var bcChannel = Math.round(Math.random()*10000);
			if(!references[referenceKey].options.subQuery) {
				references[referenceKey].options.subQuery = [[],[],[],query[3]];
				references[referenceKey].value = "";
			}
			references[referenceKey].options.bcChannel = bcChannel;
			references[referenceKey].options.bc = new BroadcastChannel("querytool"+bcChannel.toString());
			references[referenceKey].options.bc.onmessage = function (ev) {
				console.log("Received Message");
				references[referenceKey].options.subQuery = JSON.parse(JSON.stringify(ev.data.subQuery));
				references[referenceKey].value = JSON.stringify(ev.data.subQueryResult).replace(/"/g, '&quot;');
				drawQueryForm();
				printQuery();
			}
		}
		window.open("index.html?subQuery=" + references[referenceKey].options.bcChannel + "&query=" + encodeURI(JSON.stringify(references[referenceKey].options.subQuery)));
	}
	function excelExport() {
		alert("Kann eine Weile dauern..")
		sendAllRequest(true);
	}
	function removeColoumn(id) {
		function removeSearch(array) {
			for (let index = 0; index < array.length; index++) {
				const element = array[index];
				if(element.id == id) {
					array.splice(index, 1);
				} else if(Array.isArray(element.value)) {
					removeSearch(element.value)
				} else if(element.options.nestedFieldsArray) {
					removeSearch(element.options.nestedFieldsArray)
				}
			}
		}
		removeSearch(query[0])
		removeSearch(query[1])
		drawQueryForm()
		printQuery();
	}
	function searchShowFields(text) {
		showFieldsOfIndex = []
		var clusterDictionaryOfIndex = clusterdictionary.find((ele) => ele.indexName == query[3])
		fieldsOfIndex.forEach(function(field) {
			var clusterDictionaryEntryForField = clusterDictionaryOfIndex ? clusterDictionaryOfIndex.fields.find((ele) => ele.fieldName == field) : undefined;
			if((clusterDictionaryEntryForField ? clusterDictionaryEntryForField.displayName : field).match(RegExp(text, 'gi')) && !field.match(RegExp('\.keyword$')))
				showFieldsOfIndex.push(field)
		})
		drawShowFieldsInResult()
	}
	getClusterdictionary(function() {
		fillIndicesDropdown();
		fillFieldsOfIndex(function() {
			fillAggableFieldsOfIndex(function() {
				drawShowFieldsInResult();
				printQuery();
				drawQueryForm();
			});
		});
	})
	ipcRenderer.on('getQuery', function() {
		ipcRenderer.send('saveQuery', query)
	})

	/*########################################################*/
	function tabify(response, options) {
		let table;
		if (typeof (options) === 'undefined') {
			options = {
				debug: false
			}
		}

		if (response.aggregations) {
			const tree = collectBucket(response.aggregations);
			table = flatten(tree);

		} else if (response.hits) {
			table = response.hits.hits.map((d) => {
				var returnObj = d._source
				if(d.fields) {
					Object.keys(d.fields).forEach(function(field) {
						if(!returnObj[field]) {
							returnObj[field] = d.fields[field]
						}
					})
				}
				return returnObj
			});

		} else if (Array.isArray(response)) {
			table = response;

		} else {
			throw new Error("Tabify() invoked with invalid result set. Result set must have either 'aggregations' or 'hits' defined.");
		}

		if (options.debug) {
			console.log("Results from tabify (first 3 rows only):");

			// This one shows where there are "undefined" values.
			console.log(table)

			// This one shows the full structure pretty-printed.
			console.log(JSON.stringify(table.slice(0, 3), null, 2))
		}

		return table;
	}

	function collectBucket(node, stack=[]) {
		if (!node)
			return;
		
		const keys = Object.keys(node);
		
		// Use old school `for` so we can break control flow by returning.
		for(let i = 0; i < keys.length; i++) {
			const key = keys[i];
			const value = node[key];
			if (typeof value === 'object' && value !== null) {
				if ("hits" in value && Array.isArray(value.hits) && value.hits.length === 1) {
					if ("sort" in value.hits[0]) {
						value.hits[0]._source['sort'] = value.hits[0].sort[0];
					}
					return value.hits[0]._source;
				}

				if (Array.isArray(value)) {
					return extractTree(value, [...stack, key]);
				}

				// Here we are sure to have an object
				if (key === "buckets" && Object.keys(value).length > 1)
				{
					return extractBuckets(value, [...stack, key]);
				}

				return collectBucket(value, [...stack, key]);
			}

			if (key === "value" && typeof value !== "object" && stack.length === 1) {
				let collectedObject = collectBucket({[stack[0]]: value});
				node = collectedObject;
			}
		}

		return node;
	}

	function extractBuckets(buckets, stack) {
		const keys = Object.keys(buckets);
		let results = [];

		for(let i = 0; i < keys.length; i++) {
			const key = keys[i];
			const value = buckets[key];

			let currentObject = collectBucket({[key]: value});

			if (!currentObject)
				continue;

			currentObject[stack[stack.length - 2]] = key;
			results.push(currentObject)
		}

		return results;
	}

	function extractTree(buckets, stack) {
		return buckets.map((bucket) => {
			return Object.keys(bucket).reduce(function (tree, key) {
				let value = bucket[key];

				if (typeof value === "object") {
					if("value" in value){
						value = value.value;
					} else {
						value = collectBucket(value, [...stack, key]);
					}
				}

				if(key === "key"){
					key = stack[stack.length - 2]
				}
				
				tree[key] = value;
			
				return tree;
			}, {});
		});
	}

	function flatten(tree, parentNode={}){

		if (!tree)
			return [];

		if (!Array.isArray(tree))
			tree = [tree];

		return tree

			// Have the child node inherit values from the parent.
			.map((childNode) => Object.assign({}, parentNode, childNode))

			// Each node object here has values inherited from its parent.
			.map((node) => {

				// Detect properties whose values are arrays.
				const childTrees = Object.keys(node)
					.map((key) => {
						const value = node[key];
						if (Array.isArray(value)) {
							return value;
						}
						return false;
					})
					.filter((d) => d);

				switch (childTrees.length) {

					// Leaf node case, return the node.
					case 0:
						return node;

					// Non-leaf node case, recurse on the child nodes.
					case 1:
						const childTree = childTrees[0];
						if(childTree.length === 0){
							return node;
						}
						return flatten(childTree, node);
					default:
						throw new Error("This case should never happen");
				}
			})

			// Flatten the nested arrays.
			.reduce((a, b) => a.concat(b), []);
	}
	function getQueryVariable(variable)
	{
		var query = window.location.search.substring(1);
		var vars = query.split("&");
		for (var i=0;i<vars.length;i++) {
				var pair = vars[i].split("=");
				if(pair[0] == variable){return pair[1];}
		}
		return(false);
	}
	function uniqueArray(a) {
		return Array.from(new Set(a));
	}
	function flattenArray(array) {
		var output = [];
		array.forEach(function(element) {
			if(element instanceof Array) {
				element.forEach(function(elementElement) {
					output.push(elementElement);
				})
			} else {
				output.push(element);
			}
		})
		return output;
	}
